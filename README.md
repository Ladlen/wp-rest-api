<p align="center">
    <h1 align="center">PHP dev assignment (public REST API)</h1>
</p>
<hr>
<h2>Requirements</h2>
<ul>
    <li>PHP8 - compatible</li>
    <li>Support for WP core version 5.7+</li>
    <li>`ipinfo.io` is a service to get the geolocation of the IP address</li>
</ul>
<hr/>
<h2>About</h2>
<p>Plugin provides a public REST API endpoint <code>my-json/v1/localize</code> that
will output following data on <code>GET</code> request:</p>
<ol>
<li><code>current_time:datetime</code> - current server time;</li>
<li><code>remote_ip:string</code> - IP address of host that made a request;</li>
<li><code>location:string</code> - geolocation of IP address according to public database
by choice (in GPS format Latitude/Longitude);</li>
</ol>
<hr/>
<h2>Installation steps</h2>
<ol>
<li>Download the <a href="https://bitbucket.org/Ladlen/wp-rest-api/get/ab6298822950.zip">plugin .zip</a> file and make note of where on your computer you downloaded it to.</li>
<li>In the WordPress admin (<code>yourdomain.com/wp-admin</code>) go to <code>"Plugins" &gt; "Add New"</code> or click the <code>"Add New"</code> button on the main plugins screen.</li>
<li>On the following screen, click the <code>"Upload Plugin"</code> button.</li>
<li>Browse your computer to where you downloaded the plugin .zip file, select it and click the <code>"Install Now"</code> button.</li>
<li>After the plugin has successfully installed, click <code>"Activate Plugin"</code>.</li>
</ol>
<hr/>
<h2>Usage</h2>
<br>
Go to <code>yourdomain.com/wp-json/my-json/v1/localize</code> to see the result.<br>
If the <code>`location`</code> is an array and it has <code>`error`</code> key, then something went wrong.<br>
The values of `error` and `response_data` have described the error.