<?php

require_once __DIR__ . '/Instantiate.php';


class UrlHandling extends Instantiate {

	protected $locationInfo;


	public function getIp(): ?string {
		return $_SERVER['REMOTE_ADDR'];
	}

	public function getArrayFromJsonUrl( string $url ): ?array {
		try {
			if ( ( $json = @file_get_contents( $url ) ) === false ) {
				throw new Exception( "Couldn't fetch data from the URL: '" . $url . "'" );
			}
		} catch ( Throwable $e ) {
			throw new Exception( "Error parsing response data; the URL: '" . $url . "'" );
		}

		return self::getArrayFromJson( $json );
	}

	public function getArrayFromJson( string $json ): ?array {
		if ( ( $array = json_decode( $json, true ) ) === null ) {
			throw new Exception( "Wrong JSON: `$json`");
		}

		return $array;
	}
}
