<?php


class Instantiate {
	public static function inst() {
		static $inst;

		if ( ! $inst ) {
			$inst = new static;
		}

		return $inst;
	}
}
