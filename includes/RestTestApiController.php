<?php

/**
 * The file that defines the core plugin class
 */

require_once __DIR__ . '/UrlHandling.php';

/**
 * The core plugin class
 */
class RestTestApiController {

	const NAMESPACE = 'my-json/v1';

	/**
	 * @var string
	 * @see ipinfo.io
	 */
	const IPINFO_IO_TOKEN = 'e52758c19d8265';

	protected array $actionList = [];


	public function addAction( string $action ) {

		$actionFunctionName = 'action_' . $action;

		if ( ! is_callable( [ $this, $actionFunctionName ] ) ) {
			throw new \Exception( 'Wrong action name: ' . $actionFunctionName );
		}

		$this->actionList[] = $action;
		add_action( $action, [ $this, $actionFunctionName ] );

		return $this;
	}

	public function action_rest_api_init() {
		register_rest_route( self::NAMESPACE, '/localize', [
			'methods'  => 'GET',
			'callback' => function () {
				//date_default_timezone_set('Europe/Moscow');
				$url = 'http://ipinfo.io/'
				       . $this->getRemoteIp()
				       . '?token=' . urlencode( self::IPINFO_IO_TOKEN );

				try {
					$location = UrlHandling::inst()->getArrayFromJsonUrl( $url );

					$locationResult = [];

					if ( $location ) {
						if ( ! empty( $location['error'] ) ) {
							$locationResult = json_encode( [
								'error' => 'Wrong result, got error from geolocation public database.',
								'request_data'  => $location,
							] );
						} else {
							if ( ! empty( $location['loc'] ) ) {
								$locationResult = $location['loc'];
							} else {
								$locationResult = [
									'error' => "The result is wrong: no required array key `loc` found in the response",
									'response_data'  => $location,
								];
							}
						}
					} else {
						$locationResult = "Could't get the location, the result is wrong. URL: `$url`";
					}
				} catch ( Exception $e ) {
					$locationResult = [
						'error' => 'Error!',
						'response_data'  => $e->getMessage(),
					];
				}

				return [
					'current_time' => date( 'Y-m-d H:i:s e' ),
					'remote_ip'    => $this->getRemoteIp(),
					'location'     => $locationResult,
				];
			},
		] );
	}

	protected function getRemoteIp() {
		$ip = $_SERVER['REMOTE_ADDR'];
		#$ip = '91.247.192.166';
		#$ip = '127.0.0.0';
		#$ip = '256.0.0.1';

		return $ip;
	}
}
