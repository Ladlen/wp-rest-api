<?php
/**
 * Plugin Name:       WP REST TEST API
 * Description:       WP REST TEST API will provide a public REST API endpoint `my-json/v1/localize`
 * Version:           0.1
 * Author:            Dmytryy Dolgov
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

require_once __DIR__ . '/includes/RestTestApiController.php';

( new RestTestApiController() )->addAction( 'rest_api_init' );
